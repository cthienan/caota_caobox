from pyramid.authentication import AuthTktAuthenticationPolicy
from pyramid.authorization import ACLAuthorizationPolicy
from pyramid.config import Configurator
from pyramid.session import SignedCookieSessionFactory


def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    # declarations
    session_factory = SignedCookieSessionFactory('Gb53ktTE1G')
    authn_policy = AuthTktAuthenticationPolicy('5D9Pha9xyP', hashalg='sha512', timeout=36000)
    authz_policy = ACLAuthorizationPolicy()

    with Configurator(settings=settings, root_factory='caota_caobox.models.RootFactory') as config:
        config.include('.models')
        config.include('pyramid_chameleon')
        config.include('.routes')

        # configurations
        config.set_session_factory(session_factory)
        config.set_authentication_policy(authn_policy)
        config.set_authorization_policy(authz_policy)

        config.scan()
    return config.make_wsgi_app()
