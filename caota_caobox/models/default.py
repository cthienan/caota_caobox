# -*- coding: utf8 -*-
from sqlalchemy import text
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import (
    scoped_session,
    sessionmaker,
)

from zope.sqlalchemy import mark_changed

from datetime import *
import transaction

def execute_query(request, query, params):
    """Execute query and mark session as changed"""
    request.dbsession.execute(query, params)
    mark_changed(request.dbsession)
    transaction.commit()

def get_docs(request, doc_id):
    """Lire les doc"""
    if doc_id == 0:
        query = "SELECT * FROM docs ORDER BY theme, intitule;"
        results = request.dbsession.execute(query).fetchall()
    else:
        query = "SELECT * FROM docs where doc_id = :doc_id;"
        results = request.dbsession.execute(query, {'doc_id': doc_id}).first()
    return results

def get_docs_bytheme(request, theme):
    """Lire les doc"""
    if theme == 'BLOG':
        query = "SELECT * FROM docs WHERE theme=:theme ORDER BY cree_le DESC LIMIT 10;"
    else:
        query = "SELECT * FROM docs WHERE theme=:theme ORDER BY intitule;"
    results = request.dbsession.execute(query, {'theme': theme}).fetchall()
    return results

def get_docs_tags(request):
    query = "SELECT * FROM docs_tags;"
    results = request.dbsession.execute(query).fetchall()
    return results

def get_docs_themes(request):
    query = "SELECT * FROM docs_themes;"
    results = request.dbsession.execute(query).fetchall()
    return results

def get_docs_bycritere(request, critere, logged_in):
    if logged_in == None:
        query = "SELECT * FROM docs  JOIN docs_themes ON docs.theme = docs_themes.theme WHERE (docs.texte like :critere) OR (docs.intitule like :critere) AND docs_themes.visible = 'oui';"
    else:
        query = "SELECT * FROM docs  JOIN docs_themes ON docs.theme = docs_themes.theme WHERE (docs.texte like :critere) OR (docs.intitule like :critere);"
    results = request.dbsession.execute(query, {'critere': '%' + critere + '%'}).fetchall()
    return results

def update_doc(request, doc_id, new_values):
    """créér ou modifier le doc"""
    # formater les champs
    s = ''
    for param in new_values.keys():
        if s:
            s += ",%s=:%s" % (param, param)
        else:
            s = "%s=:%s" % (param, param)

    if doc_id == '0':
        query = "INSERT INTO docs SET %s;" % s
    else:
        new_values['doc_id'] = doc_id
        query = "update docs SET %s WHERE doc_id = :doc_id;" % s
    execute_query(request, query, new_values)

def delete_doc(request, doc_id):
    """supprimer la doc"""
    query = "delete from docs where doc_id = :doc_id;"
    execute_query(request, query, {'doc_id': doc_id})

