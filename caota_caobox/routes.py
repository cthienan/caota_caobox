def includeme(config):
    config.add_static_view('static', 'static', cache_max_age=3600)
    config.add_route('home', '/')
    config.add_route('apropos', 'apropos')
    config.add_route('archives', '/archives')
    config.add_route('doc_edit', '/doc_edit/{doc_id}')
    config.add_route('doc_view', '/doc_view/{doc_id}')
    config.add_route('doc_search', '/doc_search')
    config.add_route('login', '/login')
    config.add_route('logout','/logout')
