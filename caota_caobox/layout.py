# -*- coding: utf8 -*-
from pyramid_layout.layout import layout_config

@layout_config(template='templates/layout.pt')
class GlobalLayout(object):

    def __init__(self, context, request):
        self.context = context
        self.request = request
        self.home_url = request.application_url

    def isAnonymous(self):
        user = self.request.authenticated_userid
        return user is None
