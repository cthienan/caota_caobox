from pyramid.response import Response

from sqlalchemy.exc import DBAPIError

from pyramid.view import (
    view_config,
    forbidden_view_config,
)
from pyramid.security import (
    remember,
    forget,
)

from pyramid.httpexceptions import (
    HTTPFound,
    HTTPNotFound,
    HTTPForbidden,
)

from ..models.default import *


@view_config(route_name='home', renderer='../templates/home.pt')
def home(request):
    
    # lire toutes les docs
    items = get_docs_bytheme(request, 'BLOG') 
    
    return {
        'page_title': "Welcome to my Lair",
        'items': items,
    }    

@view_config(route_name='archives', renderer='../templates/archives.pt')
def archives(request):

    theme = 'blog'

    if 'theme' in request.params:
        theme = request.params["theme"]

    # lire la table des themes
    themes = get_docs_themes(request)

    # lire toutes les docs du theme
    docs = get_docs_bytheme(request, theme) 

    return {
            'page_title': "Archives de %s" % theme,
            'docs': docs,
            'themes': themes,
            'theme': theme,
        }

@view_config(route_name='doc_edit', renderer='../templates/doc_edit.pt', permission='view')
def doc_edit(request):
    doc_id = request.matchdict['doc_id']    
    url = request.route_url('doc_edit',doc_id=doc_id)

    message = ""
    themes = get_docs_themes(request)
    tags = get_docs_tags(request)
    
    if doc_id == '0':
        titre = "Nouveau doc"
        # nouveau
        doc = {}
        doc['intitule'] = ''
        doc['texte'] = ''
        doc['theme'] = ''
        doc['tag1'] = ''
        doc['tag2'] = ''
    else:
        titre = "Modifier : %s" % str(doc_id)
        doc = get_docs(request, doc_id)

    if 'form.submitted' in request.params:
        new_values = {}
        for param, db_value in doc.items():
            if param in request.params and request.params[param] != db_value:
                new_values[param] = request.params[param]

        if new_values:   
            #import pdb;pdb.set_trace()
            update_doc(request, doc_id, new_values)              
            if doc_id != '0':
                return HTTPFound(location=request.route_url('doc_view', doc_id=doc_id))
            else:
                return HTTPFound(location=request.route_url('archives',theme=request.params['theme']))

    if 'form.deleted' in request.params:
        if doc_id != '0':
            delete_doc(request, doc_id)
            request.session.flash(u"<%s> est supprimée avec succès." % doc.intitule, 'success')
            return HTTPFound(location=request.route_url('archives',theme=doc.theme))

    return {
        'page_title': titre,
        'url': url,
        'message': message,
        'doc_id': doc_id,
        'doc': doc,
        'themes': themes,
        'tags': tags,
    }

@view_config(route_name='doc_search', renderer='../templates/doc_search.pt')
def doc_search(request):

    logged_in = request.authenticated_userid
    critere = ''
    docs = []

    if 'form.submitted' in request.params:
        critere = request.params['critere']
        # si afficher tous les fiches ?
        docs = get_docs_bycritere(request, critere, logged_in)

    return {
        'page_title': "Rechercher",
        'docs': docs,
        'critere': critere,
    }

@view_config(route_name='apropos', renderer='../templates/apropos.pt')
def apropos(request):


    return {
        'page_title': "A propos",
    }


@view_config(route_name='doc_view', renderer='../templates/doc_view.pt')
def doc_view(request):
        
    logged_in = request.authenticated_userid
    doc_id = request.matchdict['doc_id']    

    doc = get_docs(request, doc_id)
    if logged_in == None and doc.theme == 'memo':
        # si anonyme, interdire de voir les memo
        return HTTPFound(location=request.route_url('home'))

    url_retour = request.route_url('archives')
    tags = doc.tag1
    if doc.tag2:
        tags += ', ' + doc.tag2
    
    # insèrer le path de static/img
    texte = doc.texte.replace('static/img/', "%s/static/img/" % request.application_url)
    
    # convertir reST en HTML
    # texte = publish_parts(texte, writer_name='html')['html_body']
    
    # convertir mardown en HTML
    from markdown2 import Markdown
    markdowner = Markdown()
    texte = markdowner.convert(texte)
    
    return {
        'page_title': doc.intitule,
        'doc_id': doc_id,
        'texte': texte,
        'modif_le': doc.modif_le.strftime('%d/%m/%Y'),
        'tags': tags,
        'url_retour':url_retour
    }

@view_config(route_name='login', renderer='../templates/login.pt', permission='view')
@forbidden_view_config(renderer='../templates/login.pt')
def login(request):

    login = ''
    login_url = request.route_url('login')

    referrer = request.url
    if referrer == login_url:
        referrer = '/' # never use the login form itself as came_from

    came_from = request.params.get('came_from', referrer)
    password = ''
    message = ''
    if 'form.submitted' in request.params:
        login = request.params['login']
        password = request.params['password']
        # mot de passe hash valide ?
        if login == "cthienan" and password == "H3lloTh3r3!":

            headers = remember(request, login)
            return HTTPFound(location=came_from, headers=headers)

        message = "Email et mot de passe invalides. La connexion a échoué."

    return {
        'page_title': "",
        'url': login_url,
        'came_from': came_from,
        'login': login,
        'message': message,
    }

@view_config(route_name='logout')
def  logout(request):
    headers = forget(request)
    request.session.flash("Vous avez bien été déconnecté", 'success')
    return HTTPFound(location=request.route_url('login'), headers=headers)